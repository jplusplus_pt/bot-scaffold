#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import telepot
from telepot.delegate import per_chat_id, create_open
from random import choice

START_MSG = "Hi, I'm a parrot, tell me stuff."
TIMEOUT = 120
TIMEOUT_MSGS = ["No one there? I'm out!",
                "Bye there!"]


class Handler(telepot.helper.ChatHandler):
    def __init__(self, seed_tuple, timeout):
        super(Handler, self).__init__(seed_tuple, timeout)
        # Add any state-keeping properties here

    def handle_message(self, msg, first=False):
        '''
        This is a slightly simpler wrapper for the open() and on_message()
        calls, which handle messages. Any state logic should also be taken
        care of here.
        '''

        # For debugging, we echo the message details locally, check the terminal
        from pprint import pprint
        pprint(msg)
        print

        if first:
            # If it's the user's first message in this session, we go here
            self.sender.sendMessage(START_MSG)
            # Don't process the first message further
            return True

        # glance2 is a Telepot convenience function that gives us the
        # metadata in a handy format
        content_type, chat_type, chat_id = telepot.glance2(msg)

        # Check that it's a text message
        if content_type != 'text':
            self.sender.sendMessage('I only understand text messages!')
            return

        # Extract the text content and send it back
        txt = msg['text']
        print "Sending back '%s'\n" % txt
        self.sender.sendMessage(txt)

    def open(self, initial_msg, seed):
        self.handle_message(initial_msg, first=True)
        return True  # prevent on_message() from being called on the initial message

    def on_message(self, msg):
        self.handle_message(msg)

    def on_close(self, exception):
        if isinstance(exception, telepot.helper.WaitTooLong):
            self.sender.sendMessage(choice(TIMEOUT_MSGS))


TOKEN = sys.argv[1]

bot = telepot.DelegatorBot(TOKEN, [
    (per_chat_id(), create_open(Handler, timeout=TIMEOUT)),
])
bot.notifyOnMessage(run_forever=True)
