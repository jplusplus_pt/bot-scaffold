Simple "Hello World" bot
========================

Run this script using your Telegram bot key as the only argument. You get the
bot key when you create a bot through the instructions over at [Telegram's
documentation](https://core.telegram.org/bots#3-how-do-i-create-a-bot).

    python bot.py <key>
