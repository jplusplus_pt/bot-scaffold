## Scope

What we're not talking about:

* Twitterbots
* Conversational UIs

What we ARE talking about:

* Scripts that use messaging apps as their UI


## Good reasons to consider bots

[A Chatbot Is Better Than a UI for a Microservice](http://www.yegor256.com/2015/11/03/chatbot-better-than-ui-for-microservice.html) is an excellent article detailing how bots can act as interfaces for simple services, with a good restaurant analogy. Summary:

* Bots serve as a layer of communication to a server, not as servers themselves
* Bots don't need to be fast: there's tolerance for slow operations
* No graphic design
* Easier to scale: "It is much easier to create a scalable client than a scalable server"
* Mistakes are less visible by the user
* History is transparently preserved
* Bots are easier to integrate with additional services


## Good articles

* DJ Adams: [Is Jabber's Chatbot the Command Line of the Future?](http://www.openp2p.com/pub/a/p2p/2002/01/11/jabber_bots.html) (2002) -- a prescient perspective on bots as command lines
* Yegor Bugayenko: [A Chatbot Is Better Than a UI for a Microservice](http://www.yegor256.com/2015/11/03/chatbot-better-than-ui-for-microservice.html) (2015) -- excellent piece with great principles for thinking of chatbots as UI
* Benedict Evans: [Chat bots, conversation and AI as an interface](http://ben-evans.com/benedictevans/2016/3/30/chat-bots-conversation-and-ai-as-an-interface) (2016) -- good explanation of how different companies (Apple, FB, MS, etc) are thinking chatbots and AI
* Dan Grover: [Bots won't replace apps. Better apps will replace apps.
](http://dangrover.com/blog/2016/04/20/bots-wont-replace-apps.html) (2016) -- a precious historical view of chat as UI (and some other thoughts about phone interfaces)
* Emmet Connolly: [Principles of bot design](https://blog.intercom.io/principles-bot-design/) -- somewhat questionable list of principles, but good reference (see also the [HN discussion](https://news.ycombinator.com/item?id=11733187))
* Bombshell: [Reading in the future: interactive fiction and chatbots](http://cettefemme-la.net/2013/11/07/reading-in-the-future-interactive-fiction-and-chatbots/) (2013) -- How the chatbot UI can augment the reading (and writing) experience


## Services and useful resources

* [Motion.ai](https://www.motion.ai/) -- "Visually build, train & deploy bots to do just about anything"
* [Wit.ai](http://wit.ai) -- Bot-oriented service for understanding input using Natural Language techniques (owned by Facebook)
* [Twine](https://twinery.org) -- Built for interactive storytelling, handy for prototyping bot logic


## Some existing bots

* [Prompt](http://www.fastcodesign.com/3057596/prompt-wants-to-be-the-one-chatbot-to-rule-them-all) -- a startup that aims to provide simple commands to popular services like flight stats or pizza delivery, with some interesting problems too (see also what [Facebook is doing with ridesharing companies](http://techcrunch.com/2015/12/16/facebook-messenger-transportation/))
* [ReplyYes](http://www.adweek.com/news/technology/how-chatbot-helped-vinyl-records-startup-make-1-million-8-months-170900) -- a successful case study of a chatbot for selling vinyl records
* [AdventureBot](https://www.reddit.com/r/TelegramBots/comments/43pa0d/bot_for_playing_zork_1_other_interactive_fiction/) -- play Zork and other interactive fiction games ([source code available](http://github.com/sneaksnake/z5bot))

## Lists of bots

* [Telegram Bot Store](https://storebot.me/top)
* [TelegramBots subreddit](http://reddit.com/r/telegrambots)
* [/r/TelegramBots bot list](https://docs.google.com/spreadsheets/d/1uQP3f2bWuPapTn_1FUcL67jW9MwLzSjysji39pmyUxY/edit#gid=2104461983)


## Introductions, tutorials and HOWTO

* [3 tools for building bots that people won’t tell to fuck off](https://medium.com/@andknf/3-tools-for-building-bots-that-people-won-t-tell-to-fuck-off-584889eaf6b9#.fuysrh8rt) -- prototype bots with Twine, Wit.ai and Twilio
* [Telegram bots: An intro for developers](https://core.telegram.org/bots) and [language bindings and examples](https://core.telegram.org/bots/samples)
* [How to build and deploy a Facebook Messenger bot with Python and Flask, a tutorial](http://tsaprailis.com/2016/06/02/How-to-build-and-deploy-a-Facebook-Messenger-bot-with-Python-and-Flask-a-tutorial/)
* [A beginner's guide to your first Slack bot](https://github.com/mccreath/isitup-for-slack/blob/master/docs/TUTORIAL.md)
* [Building a Slack Bot with Node.js](https://scotch.io/tutorials/building-a-slack-bot-with-node-js-and-chuck-norris-super-powers)
